package com.wikistrat.userselectcf.customfield;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.user.search.AssigneeService;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.UserCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.plugin.userformat.ProfileLinkUserFormat;
import com.atlassian.jira.plugin.userformat.UserFormats;
import com.atlassian.jira.plugin.userformat.UserFormatter;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserFilterManager;
import com.atlassian.jira.user.UserHistoryManager;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.wikistrat.userselectcf.dto.UserSelectOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.net.URI;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserPickerCustomFieldType extends UserCFType {
    private static final Logger log = LoggerFactory.getLogger(UserPickerCustomFieldType.class);

    private final JiraAuthenticationContext authenticationContext;
    private final AvatarService avatarService;
    private final AssigneeService assigneeService;
    private final WorkflowManager workflowManager;
    private final EmailFormatter emailFormatter;
    private final JiraBaseUrls jiraBaseUrls;
    private final UserFormatter userFormatter;

    public UserPickerCustomFieldType(@ComponentImport CustomFieldValuePersister customFieldValuePersister,
                                     @ComponentImport UserConverter userConverter,
                                     @ComponentImport GenericConfigManager genericConfigManager,
                                     @ComponentImport ApplicationProperties applicationProperties,
                                     @ComponentImport JiraAuthenticationContext authenticationContext,
                                     @ComponentImport FieldConfigSchemeManager fieldConfigSchemeManager,
                                     @ComponentImport ProjectManager projectManager,
                                     @ComponentImport SoyTemplateRendererProvider soyTemplateRendererProvider,
                                     @ComponentImport GroupManager groupManager,
                                     @ComponentImport ProjectRoleManager projectRoleManager,
                                     @ComponentImport UserSearchService searchService,
                                     @ComponentImport JiraBaseUrls jiraBaseUrls,
                                     @ComponentImport UserHistoryManager userHistoryManager,
                                     @ComponentImport UserFilterManager userFilterManager,
                                     @ComponentImport I18nHelper i18nHelper,
                                     @ComponentImport UserBeanFactory userBeanFactory,
                                     @ComponentImport AvatarService avatarService,
                                     @ComponentImport AssigneeService assigneeService,
                                     @ComponentImport WorkflowManager workflowManager,
                                     @ComponentImport EmailFormatter emailFormatter,
                                     @ComponentImport UserFormats userFormats) {
        super(customFieldValuePersister, userConverter, genericConfigManager, applicationProperties,
              authenticationContext,
              fieldConfigSchemeManager, projectManager, soyTemplateRendererProvider, groupManager, projectRoleManager,
              searchService, jiraBaseUrls, userHistoryManager, userFilterManager, i18nHelper, userBeanFactory);

        this.authenticationContext = authenticationContext;
        this.avatarService = avatarService;
        this.assigneeService = assigneeService;
        this.workflowManager = workflowManager;
        this.emailFormatter = emailFormatter;
        this.jiraBaseUrls = jiraBaseUrls;
        this.userFormatter = userFormats.formatter(ProfileLinkUserFormat.TYPE);
    }

    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue,
                                                     final CustomField field,
                                                     final FieldLayoutItem fieldLayoutItem) {
        final Map<String, Object> velocityParams = super.getVelocityParameters(issue, field, fieldLayoutItem);
        if (issue == null) {
            return velocityParams;
        }
        velocityParams.put("userFormatter", userFormatter);
        Object customFieldValue = issue.getCustomFieldValue(field);
        ApplicationUser selectedUser = null;
        if (customFieldValue != null) {
            selectedUser = (ApplicationUser) customFieldValue;
            velocityParams.put("user", selectedUser);
        }
        optionsForHtmlSelect(issue, selectedUser, velocityParams);
        return velocityParams;
    }


    private void optionsForHtmlSelect(Issue issue, ApplicationUser selectedUser,
                                      Map<String, Object> velocityParams) {
        final ApplicationUser loggedInUser = authenticationContext.getLoggedInUser();
        JiraWorkflow workflow = workflowManager.getWorkflow(issue);
        Collection<ActionDescriptor> allActions = workflow.getAllActions();
        log.debug("step.getActions() = {}", allActions);


        if (allActions.iterator().hasNext()) {
            ActionDescriptor descriptor = allActions.iterator().next();
            List<ApplicationUser> assignableUsers = assigneeService.getAssignableUsers(issue, descriptor);
            List<ApplicationUser> suggestedAssignees = assigneeService.getSuggestedAssignees(issue, loggedInUser,
                                                                                             assignableUsers);

            Map<String, Boolean> uniqueFullNames = assigneeService.makeUniqueFullNamesMap(assignableUsers);
            if (selectedUser != null) {
                velocityParams.put("selectedUser", createUserSelectOption(selectedUser, loggedInUser, uniqueFullNames));
                velocityParams.put("currentUserSelected", Objects.equals(selectedUser, loggedInUser));
            }
            Map<String, UserSelectOption> userSelectOptions = makeUserSelectOptionMap(selectedUser, assignableUsers,
                                                                                      suggestedAssignees,
                                                                                      loggedInUser, uniqueFullNames);

            velocityParams.put("userSelectOptions", userSelectOptions.values());
            velocityParams.put("issue", issue);
            velocityParams.put("baseUrl", jiraBaseUrls.baseUrl());
        } else {
            velocityParams.put("userSelectOptions", Collections.emptyList());
        }
    }

    private Map<String, UserSelectOption> makeUserSelectOptionMap(ApplicationUser selectedUser,
                                                                  List<ApplicationUser> assignableUsers,
                                                                  List<ApplicationUser> suggestedUsers,
                                                                  ApplicationUser loggedInUser,
                                                                  Map<String, Boolean> uniqueFullNames) {


        return Stream.concat(suggestedUsers.stream(), assignableUsers.stream())
                .filter(u -> !Objects.equals(u, selectedUser))
                .map(u -> createUserSelectOption(u, loggedInUser, uniqueFullNames))
                .collect(Collectors.toMap(UserSelectOption::getName, Function.identity(), (u, v) -> v,
                                          LinkedHashMap::new));
    }

    private UserSelectOption createUserSelectOption(ApplicationUser user, final ApplicationUser loggedInUser,
                                                    @Nullable Map<String, Boolean> fullNames) {
        String displayName = user.getDisplayName();
        boolean isUnique = fullNames == null || fullNames.get(displayName);
        if (!isUnique) {
            displayName += " (" + user.getName() + ")";
        }

        URI avatarURL = avatarService.getAvatarURL(loggedInUser, user, Avatar.Size.SMALL);
        String email = emailFormatter.formatEmail(user.getEmailAddress(), loggedInUser);
        return new UserSelectOption(user.getKey(), user.getName(), displayName, avatarURL.toString(), email);
    }
}