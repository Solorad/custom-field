package com.wikistrat.userselectcf.dto;

public class UserSelectOption {
    private String key;
    private String name;
    private String displayName;
    private String avatarURL;
    private String emailAddress;

    public UserSelectOption(String key, String name, String displayName, String avatarURL, String emailAddress) {
        this.key = key;
        this.name = name;
        this.displayName = displayName;
        this.avatarURL = avatarURL;
        this.emailAddress = emailAddress;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
