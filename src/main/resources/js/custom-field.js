AJS.$(function() {
  var events = require("jira/util/events");
  var reasons = require("jira/util/events/reasons");
  events.bind(JIRA.Events.NEW_CONTENT_ADDED, function(e, context, reason) {
    AJS.$("[data-fieldtypecompletekey='com.wikistrat.jira.custom-field:user-picker-custom-field']").each(function() {
      var $editInput = AJS.$(this);
      var $dd = $editInput.parents("dd");

      var currentUser = JIRA.Users.LoggedInUser.userName();

      var $userSelect = $editInput.find("div.user-hover");
      if ($userSelect.length > 0) {
        // cf view mode
        var selectedUser = $userSelect.attr("rel");
      } else {
        // cf edit  mode
        selectedUser = $editInput.find("select").val();
        // select is multiple
        if (Array.isArray(selectedUser) && selectedUser.length > 0) {
          selectedUser = selectedUser[0];
        }
      }
      var isCurrentUserSelected = currentUser === selectedUser;

      var $link = AJS.$(".wikistrat-assign-to-me-link");
      if ($link.length === 0 && !isCurrentUserSelected) {
        $dd.append(Wikistrat.CustomFields.assignToMeLink());
      } else if ($link.length > 0 && isCurrentUserSelected) {
        $link.remove();
      }
    });
  });

  AJS.$(document).on("click", ".wikistrat-assign-to-me-link", function() {
    var $link = AJS.$(this);
    var $dd = $link.parents("dd");
    var cfId = $dd.find("input.hidden-wikistrat-cf-id").val();
    var $select = $dd.find("select");
    var username = JIRA.Users.LoggedInUser.userName();
    if ($select.length) {
      $select.trigger('set-selection-value', username); // cause a change event as well as set it
      $dd.find("form").submit();
    } else {
      // use JIRA rest api to update cf value than reload issue panels
      var data = {
        fields: {}
      };
      data.fields[cfId] = {
        name: username,
        key: username
      };
      AJS.$
        .ajax({
          type: "PUT",
          url: contextPath + "/rest/api/2/issue/" + JIRA.Issue.getIssueId(),
          data: JSON.stringify(data),
          contentType: "application/json"
        })
        .done(function() {
          JIRA.trigger(JIRA.Events.REFRESH_ISSUE_PAGE, [JIRA.Issue.getIssueId()]);
        });
    }
  });
});
