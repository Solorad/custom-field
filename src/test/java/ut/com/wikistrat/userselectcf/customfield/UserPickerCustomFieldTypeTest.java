package ut.com.wikistrat.userselectcf.customfield;

import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.user.search.AssigneeService;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.plugin.userformat.ProfileLinkUserFormat;
import com.atlassian.jira.plugin.userformat.UserFormats;
import com.atlassian.jira.plugin.userformat.UserFormatter;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.UserFilterManager;
import com.atlassian.jira.user.UserHistoryManager;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.workflow.WorkflowManager;
import com.wikistrat.userselectcf.customfield.UserPickerCustomFieldType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserPickerCustomFieldTypeTest {
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private JiraBaseUrls jiraBaseUrls;
    @Mock
    private AvatarService avatarService;
    @Mock
    private AssigneeService assigneeService;
    @Mock
    private WorkflowManager workflowManager;
    @Mock
    private EmailFormatter emailFormatter;
    @Mock
    private UserFormats userFormats;
    @Mock
    private UserFormatter userFormatter;

    private UserPickerCustomFieldType userPickerCustomFieldType;

    @Before
    public void setUp() {
        when(userFormats.formatter(ProfileLinkUserFormat.TYPE)).thenReturn(userFormatter);
        userPickerCustomFieldType = new UserPickerCustomFieldType(mock(CustomFieldValuePersister.class),
                                                                  mock(UserConverter.class),
                                                                  mock(GenericConfigManager.class),
                                                                  mock(ApplicationProperties.class),
                                                                  authenticationContext,
                                                                  mock(FieldConfigSchemeManager.class),
                                                                  mock(ProjectManager.class),
                                                                  mock(SoyTemplateRendererProvider.class),
                                                                  mock(GroupManager.class),
                                                                  mock(ProjectRoleManager.class),
                                                                  mock(UserSearchService.class),
                                                                  jiraBaseUrls,
                                                                  mock(UserHistoryManager.class),
                                                                  mock(UserFilterManager.class),
                                                                  mock(I18nHelper.class),
                                                                  mock(UserBeanFactory.class),
                                                                  avatarService,
                                                                  assigneeService,
                                                                  workflowManager,
                                                                  emailFormatter,
                                                                  userFormats);
    }

    @Test
    public void getVelocityParameters() throws Exception {
    }

}